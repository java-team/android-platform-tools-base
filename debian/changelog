android-platform-tools-base (2.0.0-1) UNRELEASED; urgency=medium

  [ Markus Koschany ]
  * Remove myself from Uploaders.

  [ Kai-Chung Yan ]
  * New upstream release
  * Use get-orig-source to generate upstream tarball
    - Refactor the upstream file structure based on that
  * New Build-Depends:
    - libandroid-databinding-java
    - antlr3
  * d/copyright: Exclude more binary files and non-free files
  * d/control:
    - Unify Maintainer to "Android Tools Maintainers"
    - Add myself to Uploaders
  * New Inception-Date.patch: Manually set Inception-Date in some JARs' manifest
    for reproducibility
  * New packages:
    * libgradle-android-plugin-java(-doc)
    * libandroid-ddms-java
    * libandroid-layoutlib-api-java
    * libandroid-tools-dvlib-java
    * libandroid-tools-repository-java
  * No longer build the folowwing as they are unused:
      * chartlib.jar
      * generate-local-data.jar
      * lint-tests.jar
      * nin9patch.jar
      * perflib.jar
      * profile.jar
      * rule-api.jar
  * No longer build against libandroid-tools-common-java or
    libandroid-tools-annotations-java which introduces self-dependencies
  * Install JARs into maven-repo
  * Generate Class-Paths for the Java programs and store them in
    /usr/share/android-platform-tools-base

 -- Kai-Chung Yan <seamlikok@gmail.com>  Mon, 01 Aug 2016 15:48:04 +0800

android-platform-tools-base (1.5.0-4) unstable; urgency=medium

  * No-change rebuild because of bug #805228.

 -- Markus Koschany <apo@debian.org>  Thu, 14 Jul 2016 23:28:15 +0200

android-platform-tools-base (1.5.0-3) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.8.
  * Build-Depend on libhttpcore-java.
  * Add libhttpcore-java-ThreadSafe.patch.
    Replace removed ThreadSafe annotation class with custom built annotations.
    Thanks to Chris Lamb for the report. (Closes: #828813)
  * Remove jlibs files and javahelper build-dependency. Make use of
    maven_repo_helper's --java-lib option instead.
  * Build-Depend on Groovy and update build.gradle.patch to fix build failure
    Cannot infer Groovy class path because no Groovy Jar was found on class
    path.

 -- Markus Koschany <apo@debian.org>  Wed, 13 Jul 2016 21:34:58 +0200

android-platform-tools-base (1.5.0-2) unstable; urgency=medium

  * Drop disable-lint.patch and build Lint because the new build-dependency
    liblombok-ast-java is available now.
  * Add liblombok-ast-java to Build-Depends.
  * Update package description and mention Android's Lint tool.

 -- Markus Koschany <apo@debian.org>  Wed, 23 Mar 2016 14:08:28 +0100

android-platform-tools-base (1.5.0-1) unstable; urgency=medium

  * Initial release. (Closes: #814797)

 -- Markus Koschany <apo@debian.org>  Thu, 18 Feb 2016 00:47:02 +0100
